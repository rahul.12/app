module.exports = function (userModel, reqBodyObj) {

    if (reqBodyObj.name) {
        userModel.name = reqBodyObj.name;
    }

    if (reqBodyObj.username) {
        userModel.username = reqBodyObj.username;
    }

    if (reqBodyObj.password) {
        userModel.password = reqBodyObj.password;
    }

    if (reqBodyObj.email) {
        userModel.email = reqBodyObj.email;
    }

    if (reqBodyObj.gender) {
        userModel.gender = reqBodyObj.gender;
    }

    if (reqBodyObj.role) {
        userModel.role = reqBodyObj.role;
    }

    if (reqBodyObj.status) {
        userModel.status = reqBodyObj.status;
    }

    if (reqBodyObj.phoneNumber) {
        userModel.phoneNumber = reqBodyObj.phoneNumber;
    }

    if (reqBodyObj.tem_address) {
        userModel.address.tem_address = reqBodyObj.tem_address;
    }

    if (reqBodyObj.per_address) {
        userModel.address.per_address = reqBodyObj.per_address;
    }

    // console.log("userModel from inputhelper is>>", userModel);
    return userModel;
}