//mongodb setup variables
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const connectionUrl = 'mongodb://localhost:27017';
const dbName = 'group26db'
const colName = 'users'
const oid = mongodb.ObjectID
module.exports = {
    mongodb,
    MongoClient,
    dbName,
    colName,
    connectionUrl,
    oid
}