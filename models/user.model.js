const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    //db modeling
    name: String,
    phoneNumber: Number,
    username: {
        type: String,
        unique: true,
        lowercase: true,
        required: true,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true,
    },
    password: {
        type: String,
        required: true,
    },
    address: {
        tem_address: [String],
        per_address: String,
    },
    dob: Date,
    gender: {
        type: String,
        enum: ['male', 'female', 'others'],
    },
    country: String,
    role: {
        type: Number,
        default: 2,
    },
    status: {
        type: String,
        default: 'active',
    },
    passwordResetExpiry:Date
});

const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;