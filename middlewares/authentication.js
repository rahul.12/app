const jwt = require('jsonwebtoken')
const configs = require('./../configs/index')
const UserModel = require('./../models/user.model');


module.exports = function (req, res, next) {
    
    var token;
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token']
    }
    if (req.headers['token']) {
        token = req.headers['token'];
    }
    if (req.query.token) {
        token = req.query.token;
    }

    if (token) {
        jwt.verify(token, configs.jwtsecret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            UserModel
                .findOne({ _id: decoded._id })
                .then(function (user) {
                    if(!user)
                    {
                        return next({
                            msg:"User Not Found!",
                            status:404,
                        })
                    }
                        req.user = user;
                        next();
                })
        })
    } else {
        next({ msg: "Token not available" })
    }
}