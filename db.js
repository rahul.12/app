const mongoose = require('mongoose');
const dbconfigs = require('./configs/database.configs');

const conexUrl = dbconfigs.connectionUrl + '/' + dbconfigs.dbName;

mongoose.connect(conexUrl, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

mongoose.connection.once("open",function(){
	console.log("database connection established")
})
mongoose.connection.on("error",function(err){
	console.log("error in connecting to server",err)
})