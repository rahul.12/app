const productQuery = require("./product.query");
const fs = require("fs");
const path = require("path");
const mapProductReq=productQuery.mappingData


function get(req, res, next) {
  var condition = {};
  if(req.user.role!==1){
    condition.vendor=req.user._id
  }
  productQuery
    .find(condition)
    .then(data => {
      if(!data){
        return next({
          msg:"Product Not Found!",
          status:404
        })
      }
      res.json(data)
    })
    .catch(err => next(err));
}

function getById(req, res, next) {
  var condition = { _id: req.params.id };
  productQuery
    .find(condition)
    .then(data => {
      if(!data){
        return next({
          msg:"Product Not Found!",
          status:404
        })
      }
      res.json(data)
    })
    .catch(err => next(err));
}

function insert(req, res, next) {
  const data = req.body;
  console.log("req.body",data)
  console.log("req.file",req.file)
  console.log("req.files",req.files)

  if (req.fileErr) {
    return next({
      msg: req.fileErr,
      status: 400,
    });
  }

  if (req.file) {
    data.image = req.file.filename;
  }

  if (req.files) {
    data.image = req.files.map(function(file,i){
      return file.filename
    });
  }

  req.body.vendor = req.user._id;
  productQuery
    .insert(data)
    .then(data => {
      res.json(data)
    })
    .catch(err => {
     return next(err)
    });
}

function update(req, res, next) {
  var data = req.body;
  data.user = req.user._id;
  req.body.vendor = req.user._id;
  //file system uploading
  if (req.fileErr) {
    return next({
      msg: req.fileErr,
      status: 400,
    });
  }
  if (req.file) {
    data.image = req.file.filename;
  }
  if (req.files) {
    data.image = req.files.map(function(file,i){
      return file.filename
    });
  }

  var productId = req.params.id;
  productQuery
    .update(productId, data)
    .then((product) => {
      if (req.file) {
        fs.unlink(
          path.join(process.cwd(), "uploads/images/" + product.oldImage),
          function (err) {
            if (!err) {
              console.log("File removed");
            }
          }
        );
      }
      res.json(product);
    })
    .catch((err) => next(err));
}

function remove(req, res, next) {
  const productId = req.params.id;
  productQuery.remove(productId)
  .then(product=>{
    res.json(product);
  })
  .catch(err=>{
    return next(err);
  })
}

function search(req, res, next) {
  console.log("req.body",req.body)
  const condition={}
  const searchCondition=mapProductReq(condition,req.body)
  console.log("searchCondition",searchCondition)
  if(req.body.minPrice){
    searchCondition.price={
      $gte:req.body.minPrice
    }
  }
  if(req.body.maxPrice){
    searchCondition.price={
      $lte:req.body.maxPrice
    }
  }
  if(req.body.minPrice&&req.body.maxPrice){
    searchCondition.price={
      $gte:req.body.minPrice,
      $lte:req.body.maxPrice
    }
  }
  if(req.body.fromDate && req.body.toDate){
    const fromDate=new Date(req.body.fromDate).setHours(0,0,0,0);
    const toDate=new Date(req.body.toDate).setHours(23,59,59,999);
    searchCondition.createdAt={
      $gte:new Date(fromDate),
      $lte:new Date(toDate)
    }
  }

  productQuery
    .find(searchCondition)
    .then((product) => res.json(product))
    .catch((err) => next(err));
}

module.exports = {
  get,
  getById,
  insert,
  update,
  remove,
  search,
};
