const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//review schema model for product
const reviewSchema = new Schema({
    point: Number,
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    }
}, {
    timestamps: true
})

//product schema model
const productSchema = new Schema({
    name: String,
    description: String,
    price: Number,
    color: String,
    brand: String,
    size: String,
    category: {
        type: String,
        required: true,
    },
    image: [String],
    condition: {
        type: String,
        enum: ["Brand new", "Used"],
        default: "Brand new",
    },
    status: {
        type: String,
        enum: ["available", "sold", "out of stock"],
        default: 'available',
    },
    modelno: String,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    warrentyStatus: Boolean,
    warrentyPeriod: String,
    discount: {
        discountedItem: Boolean,
        discountType: String,
        discountValue: Number,
    },
    tags: [String],
    reviews: [reviewSchema],
    offers: [String],
    couponCode: String,
}, {
    timestamps: true,
})

module.exports = mongoose.model('product', productSchema);