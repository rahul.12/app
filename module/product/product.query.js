const productModel = require("./../product/product.model");

function mappingData(product, productDetail) {
  if (productDetail.name) {
    product.name = productDetail.name;
  }

  if (productDetail.description) {
    product.description = productDetail.description;
  }

  if (productDetail.price) {
    product.price = productDetail.price;
  }

  if (productDetail.brand) {
    product.brand = productDetail.brand;
  }

  if (productDetail.vendor) {
    product.vendor = productDetail.vendor;
  }

  if (productDetail.size) {
    product.size = productDetail.size;
  }

  if (productDetail.category) {
    product.category = productDetail.category;
  }

  if (productDetail.image) {
    product.image=typeof(productDetail.image)==="string"?productDetail.image.split(','):productDetail.image;
  }

  if (productDetail.condition) {
    product.condition = productDetail.condition;
  }

  if (productDetail.status) {
    product.status = productDetail.status;
  }

  if (productDetail.modelno) {
    product.modelno = productDetail.modelno;
  }

  if (productDetail.warrentyStatus) {
    product.warrentyStatus = productDetail.warrentyStatus;
  }

  if (productDetail.warrentyPeriod) {
    product.warrentyPeriod = productDetail.warrentyPeriod;
  }

  if (productDetail.discountedItem) {
    product.discount.discountedItem = productDetail.discountedItem;
  }

  if (productDetail.discountType) {
    product.discount.discountType = productDetail.discountType;
  }

  if (productDetail.discountValue) {
    product.discount.discountValue = productDetail.discountValue;
  }

  if (productDetail.tags) {
    product.tags = typeof(productDetail.tags)==="string"?productDetail.tags.split(','):productDetail.tags;
  }

  if (productDetail.offers) {
    product.offers = typeof(productDetail.offers)==="string"?productDetail.offers.split(','):productDetail.offers;
  }

  if (productDetail.couponCode) {
    product.couponCode = productDetail.couponCode;
  }

  if (productDetail.reviewpoint && productDetail.reviewmessage) {
    let review = {
      point: productDetail.reviewpoint,
      message: productDetail.reviewmessage,
      user: productDetail["user"],
    };
    product.reviews.push(review);
  }
// console.log("product mapping is",product);
  return product;
}


function insert(data) {
  var newProduct = new productModel({});
  //mapping data here
  mappingData(newProduct, data);
  console.log("i am product",newProduct)
  return newProduct.save();
}

function find(condition) {
  return productModel
  .find(condition)
  .populate("vendor",{username:1})
  .sort({_id:-1})
  .exec();
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    productModel.findById(id, function (err, product) {
      if (err) {
        return reject(err);
      }
      if (!product) {
        return reject({
          msg: "Product not found!",
        });
      }
      var oldImage = product.image[0];
      mappingData(product, data);
      product.save(function (err, product) {
        if (err) {
          reject(err);
        } else {
          product.oldImage = oldImage;
          resolve(product);
        }
      });
    });
  });
}

function remove(id) {
 return new Promise(function(resolve,reject){
  productModel.findById(id, function (err, product) {
    if (err) {
      reject(err)
    } 
    product.remove(function(err,product){
      if(err){
        return reject(err)
      }
      if(!product){
       reject({
        msg:"Product Not Found!",
        status:404
      })
    }
    return resolve(product)
    })
  });
 }) 
}

module.exports = {
  insert,
  find,
  update,
  remove,
  mappingData
};
