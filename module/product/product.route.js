const productCtrl = require("./product.controller");
const Authenticate = require("./../../middlewares/authentication");
const router = require("express").Router();
const uploader = require("./../../middlewares/uploaders.js");

/*
/######### FILE OPERATION PERFORMED IN SERPERATE FILE############ 

// const uploads = multer({
//   dest: path.join(process.cwd(), "uploads"),
// });
// function filter(req, file, cb) {
//   const mimetype = file.mimetype.split("/")[0];
//   if (mimetype === "image") {
//     cb(null, true);
//   } else {
//     req.fileErr = "Invalid file formate";
//     cb(null, false);
//   }
// }

// const diskStorage = multer.diskStorage({
//   filename: function (req, file, cb) {
//     cb(null, Date.now() + "-" + file.originalname);
//   },
//   destination: function (req, file, cb) {
//     cb(null, path.join(process.cwd(), "uploads/images"));
//   },
// });
// const upload = multer({
//   storage: diskStorage,
//   fileFilter: filter,
// });

###################***##################
*/

router
  .route("/")
  .get(Authenticate, productCtrl.get)
  .post(uploader.array("image"),Authenticate, productCtrl.insert);

router.route("/search")
	.get(productCtrl.search)
	.post(productCtrl.search)

router
  .route("/:id")
  .get(Authenticate, productCtrl.getById)
  .delete(Authenticate, productCtrl.remove)
  .put(uploader.array("image"), Authenticate, productCtrl.update);

module.exports = router;
