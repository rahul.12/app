const events = require("events")
const myEvents = new events.EventEmitter();

myEvents.on("sayHi", function (name) {
    console.log(`hi,${name} how are you ?`);
})

myEvents.emit("sayHi", "Rahul")