const socket = require("socket.io")
var users = []
module.exports = function (app, configs) {
    const io = socket(app.listen(configs.socketPort, function (err) {
        if (err) {
            console.log("error in listening socket port")
        }
        else {
            console.log("Server connected to Socket Port at" + configs.socketPort)
        }
    }))
    io.on("connection", function (client) {
        var id = client.id
        client.on("new-msg", function (data) {
            console.log("backend Data is >>", data)
            client.emit("reply-msg-own", data)//for own only
            client.broadcast.to(data.receiverId).emit("reply-msg", data)//for every client except own
        })
        client.on("new-user", function (data) {
            users.push({
                id,
                name: data
            })
            console.log(users)
            client.emit("users", users)
            client.broadcast.emit("users", users)
        })
        client.on("disconnect", function () {
            users.forEach(function (user, i) {
                if (user.id === id) {
                    users.splice(i, 1)
                }
            })
            client.broadcast.emit("users", users)
        })
    })

}