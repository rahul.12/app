const router = require('express').Router();

const productRouter = require('./../module/product/product.route');
const authRouter = require('./../controller/auths');
const userRouter = require('./../controller/user');
const Authenticate = require('./../middlewares/authentication');

router.use('/auth', authRouter);
router.use('/user', Authenticate, userRouter);
router.use('/product', productRouter);

module.exports = router;