const express = require('express');
const app = express();
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');
const configs = require('./configs/index')

const { urlencoded, json } = require('express');
//db.js file running ||
require('./db')


//template engine setup
// const pug = require('pug');
// app.set('view engine');
// app.set('views', path.join(__dirname, 'views'))

//Router 
const APIrouter = require('./route/api.route')


//socket.io work here
require('./socket')(app, configs)

//thirdparty middleware
app.use(morgan('dev'));
app.use(cors());

//parsing data
app.use(urlencoded({ extended: true }));
app.use(json());

//impoerting image
app.use(express.static('uploads'));
app.use('/file', express.static(path.join(__dirname, 'uploads')));
//app.use('/image', express.static('uploads')); 

//now routing path for different URL
app.use('/api', APIrouter);

//incase error path(error handling middleware)>>> Handles all errors
app.use(function (err, req, res, next) {
    res.status(400)
    res.json({
        msg: err.msg || err,
        status: err.status || 404
    })
})

//listen to port
app.listen(configs.port, function (err) {
    if (err) {
        console.log('error in connection');
    } else {
        console.log('connected to port at ' + configs.port);
    }
});