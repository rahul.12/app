const router = require('express').Router();
const MapUser = require('./../helper/User.model.helper');
// const { json } = require('express');

//importing UserModel
const UserModel = require('./../models/user.model');

//diff routes
router.route('/')
    .get(function (req, res, next) {
        //    if (req.user.role !== 1) {
        //         return next({ msg: "You arenot a admin" })
        //     }

        UserModel
            .find({})
            .sort({ _id: -1 })
            .exec(function (err, result) {
                if (err) {
                    return next(err)
                }
                res.json(result);
            })
    });


router.route('/:id')
    .get(function (req, res, next) {
        UserModel
            .findById(req.params.id, function (err, user) {
                if (err) {
                    return next(err)
                }
                if (!user) {
                    return next({
                        msg: "User Not Found!",
                        status: 404
                    })
                }
                res.json(user)
            })
    })
    .put(function (req, res, next) {
        UserModel
            .findById(req.params.id, function (err, user) {
                if (err) {
                    return next(err)
                }
                if (!user) {
                    return next({
                        msg: "User Not Found!",
                        status: 404,
                    })
                }
                var MappedUser = MapUser(user, req.body);
                MappedUser.save(function (err, updated) {
                    if (err) {
                        return next(err);
                    }
                    res.json(updated);
                })
            })
    })
    .delete(function (req, res, next) {
        /* if (req.user.role !== 1) {
             return next({
                 msg: "permission denied!"
             })
         }
         */
        UserModel
            .findOne({ _id: req.params.id })
            .then(user => {
                if (!user) {
                    return next({
                        msg: "User Not Found!",
                        status: 404
                    })
                }
                user.remove(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    res.json(user);
                })
            })
            .catch(err => next(err))
    })

module.exports = router;