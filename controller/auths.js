const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helper/User.model.helper')
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const configs = require('./../configs/index');
const {json}=require("express")
const nodemailer=require("nodemailer")

const sender=nodemailer.createTransport({
    service:"Gmail",
    auth:{
        user:"rt4463999@gmail.com",
        pass:"rollno15"
    }
})

function prepareMail(data){
    return {
    from: '"Group26 API 👻" <noreply@something.com>', // sender address
    to:"RahulTestCode01@gmail.com,"+data.email, // list of receivers
    subject: "Forget Password ✔", // Subject line
    text: "Forget Password?", // plain text body
    html: `
        <p><strong>Hello ${data.name}</strong></p>
        <p>we notice that you are having trouble with your password,please visit this link to reset the password</p>
        <p><strong><a href="${data.link}">Click here to reset password</a></strong></p>
        `, // html body
  }
}

function createToken(data) {
    return jwt.sign({ _id: data._id }, configs.jwtsecret);
}

router.route('/login')
    .post(function (req, res, next) {
        UserModel
            .findOne({
                $or:[{ username: req.body.username },
                {email:req.body.username}]
            }, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: "Invalid username",
                        status: 404
                    });
                }
                var isMatch = passwordHash.verify(req.body.password, user.password);
                if (isMatch) {
                    var token = createToken(user);
                    res.json({
                        user,
                        token
                    });
                } else {
                    return next({
                        msg: "Invalid password",
                        status: 404
                    });
                }
            })
    });

router.route('/register')
    .post(function (req, res, next) {
        console.log("Req.body is", req.body);
        const newUser = new UserModel({});

        //data storing in newUser-->>one of the way for data storing
        // newUser.name = req.body.name;
        // newUser.username = req.body.username;
        // newUser.password = req.body.password;
        // newUser.email = req.body.email;
        // newUser.role = req.body.role;
        // newUser.status = req.body.status;

        const user = MapUser(newUser, req.body);
        console.log("newUser is", newUser);
        newUser.password = passwordHash.generate(req.body.password);
        user.save(function (err, user) {
            if (err) {
                console.log("error is >>", err);
                return next(err);
            }
            res.json(user);
        })
        // newUser.save()
        // .then(user=>{res.json(user)})
        // .catch(err=>{
        //     console.log("error is>>",err)
        // })
    });

router.route("/forget-password")
    .post(function(req,res,next){
        var email=req.body.email;
        UserModel.findOne({email:email},function(err,user){
            if(err){
                return next(err)
            }
            if(!user){
                return next({
                    msg:"Email Not Found",
                    status:404
                })
            }

            var data={
                name:user.username,
                email,
                link:`${req.headers.origin}/reset-password/${user._id}`
            }

            const emailData=prepareMail(data);
            user.passwordResetExpiry=new Date().getTime()+(1000*60*60*2)
            user.save(function(err,user){
                if(err){
                    return next(err)
                }
                sender.sendMail(emailData,function(err,done){
                if(err){
                    // console.log("sender error>>",err)
                    return next(err)
                }
                res.json(done)
            })
            })
            
        })
    })

    router.route("/reset-password/:id")
    .post(function(req,res,next){
        UserModel
        .findOne({_id:req.params.id,
            passwordResetExpiry:{
                $gte:Date.now()
            }})
        .then(user=>{
            if(!user){
                return next({
                    msg:"Password reset link Expired*",
                    status:400
                })
            }
            user.password=passwordHash.generate(req.body.password)
            user.passwordResetExpiry=null
            user.save(function(err,done){
                if(err){
                    return next(err)
                }
                res.json(done)
            })
        })
        .catch(err=>next(err))
    })
module.exports = router;