const app = require('express')();
const fs = require('fs');

app.get('/write/:filename/:content', function (req, res) {
    fs.writeFile(req.params.filename, req.params.content, function (err) {
        if (err) {
            res.json({
                msg: err,
                params: req.params,
                query: req.query,
            });
            return;
        }
        res.json({
            msg: 'File writing success',
            params: req.params,
            query: req.query,
        })
    })
});

app.get('/read/:filename', function (req, res) {
    fs.readFile(req.params.filename, 'UTF-8', function (err, data) {
        if (err) {
            res.json({
                msg: err,
                params: req.params,
                query: req.query,
            });

        } else {
            res.json({
                msg: data,
                params: req.params,
                query: req.query,
            });
        }
    })
})

app.get('/remove/:filename', function (req, res) {
    fs.unlink(req.params.filename, function (err) {
        if (err) {
            res.json({
                msg: err,
                params: req.params,
                query: req.query,
            });

        } else {
            res.json({
                msg: req.params.filename + ' ' + 'Deleted',
                params: req.params,
                query: req.query,
            })
        }
    })
})

app.get('/rename/:oldpath/:newpath', function (req, res) {
    fs.rename(req.params.oldpath, req.params.newpath, function (err) {
        if (err) {
            res.json({
                msg: err,
                params: req.params,
                query: req.query,
            })
        } else {
            res.json({
                msg: 'file ' + req.params.oldpath + ' is changed to ' + req.params.newpath,
                params: req.params,
                query: req.query,
            })
        }
    })
})

app.listen(9090, function (err, done) {
    if (err) {
        console.log('Not connected to server');
    } else {
        console.log('server connected at port 9090');
    }
})